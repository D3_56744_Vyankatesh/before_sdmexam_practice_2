const mysql = require('mysql2')
const pool = mysql.createPool({
    host: 'db',
    password: 'root',
    user: 'root',
    port: 3306,
    database: 'mydb',
    waitForConnections: true,
    connectionLimit: 10,
    queueLimit: 0,
})

module.exports = pool