const express = require('express')
const db = require('../db')
const util = require('../utils')
const router = express.Router()

router.post('/', (request, response)=>{
    const {title, description, price} = request.body
    const statement = `insert into product (title, description, price) 
                        values('${title}', '${description}', '${price}')`

    db.execute(statement, (error, result)=>{
        response.send(util.createResult(error, result))
    })
})

router.get('/', (request, response)=>{
    const statement = `select title, description, price from product`

    db.execute(statement, (error, result)=>{
        response.send(util.createResult(error, result))
    })
})

router.put('/:id', (request, response)=>{
    const {title, description, price} = request.body
    const {id} = request.params
    const statement = `update product set 
                        title = '${title}', description = '${description}', price =${price} 
                        where id = ${id}`

    db.execute(statement, (error, result)=>{
        if(result['affectedRows'] == 0){
            response.send('product not found', null)
            return
        }
        response.send(util.createResult(error, result))
    })
})

router.delete('/:id', (request, response)=>{
    const {id} = request.params
    const statement = `delete from product where id = ${id}`

    db.execute(statement, (error, result)=>{
        if(result['affectedRows'] == 0){
            response.send(util.createResult('product not found', null))
            return
        }
        response.send(util.createResult(error, result))
    })
})

module.exports = router