const express = require('express')
const app = express()
app.use(express.json())

const routerProduct = require('./routes/product')
app.use('/product', routerProduct)


app.listen(4000, () => {
    console.log('server startet at port 4000');
})