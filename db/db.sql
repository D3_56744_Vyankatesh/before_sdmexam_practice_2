CREATE TABLE product (
  id INTEGER PRIMARY KEY auto_increment,
  title VARCHAR(100),
  description VARCHAR(300),
  price FLOAT,
  category_id INTEGER
);

-- CREATE TABLE category (
--   id INTEGER PRIMARY KEY auto_increment,
--   title VARCHAR(100),
--   description VARCHAR(300)
-- );

-- INSERT INTO category (title, description) VALUES ('Electronics', 'Electronics products');
-- INSERT INTO category (title, description) VALUES ('Computers', 'List of Computers');
-- INSERT INTO category (title, description) VALUES ('TV', 'Televisions');
-- INSERT INTO category (title, description) VALUES ('Gift', 'Gifts');
-- INSERT INTO category (title, description) VALUES ('Fashion', 'Men and Women fashion accessories');